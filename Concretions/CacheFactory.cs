﻿using System;
using System.Collections.Generic;

namespace Ncache
{
	public class CacheFactory :  ICacheFactory
	{
		protected static ICacheFactory _CacheFactory;

		protected CacheFactory () {}

		public static ICacheFactory GetInstance()
		{
			if (_CacheFactory == null)
			{
				_CacheFactory = new CacheFactory ();
			}
			return _CacheFactory;
		}

		public ICache Create(int maxSize,
			CacheRemovalType removalType = CacheRemovalType.Random)
		{
			ICache ReturnCache = null;

			switch (removalType) {
				case CacheRemovalType.Random:
					ReturnCache = new RandomCache (maxSize);
					break;
				case CacheRemovalType.LeastRecentlyUsed:
					ReturnCache = new LeastRecentlyUsedCache (maxSize);
					break;
			}
			return ReturnCache;
		}
	}
}

