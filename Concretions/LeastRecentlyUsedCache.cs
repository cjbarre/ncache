﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ncache
{
	public class LeastRecentlyUsedCache : BaseMemoryCache, ICache
	{
		public LeastRecentlyUsedCache (int maxSize) : base (maxSize)
		{
		}

		public override object Get (object key)
		{
			object returnObject = null;

			if (Exists (key))
			{
				var entry = _storage [key];
				if (entry != null)
				{
					entry.LastUsed = DateTime.Now;
					returnObject = entry.Item;
				}
			}
			return returnObject;
		}

		public override void Add (object key, object value)
		{
			if (CacheIsFull ())
			{
				Object leastRecentlyUsedItemKey = GetLeastRecentlyUsedItemKey ();
				Remove (leastRecentlyUsedItemKey);
			}
			_storage.Add (key, new CacheEntry (value));
		}

		public override bool Exists (object key)
		{
			var returnValue = false;

			if (_storage.ContainsKey (key))
			{
				var entry = _storage [key];
				if (entry != null)
				{
					entry.LastUsed = DateTime.Now;
					returnValue = true;
				}
			}
			return returnValue;
		}

		private Object GetLeastRecentlyUsedItemKey ()
		{
			return (from entry in _storage
			        let oldestEntry = _storage.Values.Min (x => x.LastUsed)
			        where entry.Value != null && entry.Value.LastUsed == oldestEntry
			        select entry.Key).FirstOrDefault ();
		}
	}
}

