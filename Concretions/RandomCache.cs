﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ncache
{
	public class RandomCache : BaseMemoryCache, ICache
	{
		public RandomCache (int maxSize) : base (maxSize)
		{
		}

		public override void Add (object key, object value)
		{
			if (CacheIsFull ())
			{
				var randomEntry = GetRandomEntry ();
				Remove (randomEntry);
			}
			_storage.Add (key, new CacheEntry (value));
		}

		private object GetRandomEntry ()
		{
			var randomizer = new Random ();
			int randomIndex = randomizer.Next (_maxSize);

			return _storage.ToArray () [randomIndex].Key;

//			return (from entry in entries
//			        let randomizer = new Random ()
//			        let randomIndex = randomizer.Next (_maxSize)
//			        select entries [randomIndex].Key).FirstOrDefault ();
		}
	}
}

