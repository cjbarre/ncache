﻿using System;

namespace Ncache
{
	public interface ICache
	{
		object Get(object key);
		bool Exists(object key);
		void Add(object key, object value);
		void Remove(object key);
	}
}

