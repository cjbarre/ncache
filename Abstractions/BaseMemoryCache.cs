﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;


namespace Ncache
{
	public abstract class BaseMemoryCache : ICache
	{
		protected IDictionary<object, CacheEntry> _storage;
		public int _maxSize { get; protected set; }

		public abstract void Add(object key, object value);

		protected BaseMemoryCache(int maxSize)
		{
			if (maxSize <= 0) {
				throw new ArgumentException ("Max size must be greater than 0");
			}
			this._maxSize = maxSize;
			this._storage = new ConcurrentDictionary<object, CacheEntry> ();
		}

		public bool CacheIsFull()
		{
			return _storage.Count.Equals (_maxSize) ? true : false;
		}
			
		public virtual Object Get(object key)
		{
			object returnObject = null;

			if (this.Exists (key)) {
				returnObject = _storage [key];
			}
			return returnObject;
		}

		public virtual bool Exists(object key)
		{
			return _storage.ContainsKey (key) ? true : false;
		}

		public void Remove(object key)
		{
			_storage.Remove(key);
		}
	}
}

