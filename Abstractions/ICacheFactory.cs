﻿using System;
using System.Collections.Generic;

namespace Ncache
{
	public interface ICacheFactory
	{
		ICache Create(int maxSize,
			CacheRemovalType removalType = CacheRemovalType.Random);
	}
}

