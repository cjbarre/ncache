﻿using System;

namespace Ncache
{
	public class CacheEntry
	{
		public Object Item { get; set;}
		public DateTime LastUsed { get; set;}

		public CacheEntry(Object item, DateTime lastUsed)
		{
			this.Item = item;
			this.LastUsed = lastUsed;
		}

		public CacheEntry(Object item)
		{
			this.Item = item;
			this.LastUsed = DateTime.Now;
		}
	}
}

