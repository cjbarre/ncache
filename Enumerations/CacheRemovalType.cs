﻿using System;

namespace Ncache
{
	public enum CacheRemovalType
	{
		LeastRecentlyUsed,
		Random
	}
}

